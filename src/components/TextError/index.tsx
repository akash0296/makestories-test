import React from 'react'

import { Error } from 'assets/icons'
import { ErrorText } from './TextError.styled'

const TextError = ({ error }: { error?: string }) => {
	if (!error) return null
	return (
		<ErrorText>
			<Error />
			<p>{error}</p>
		</ErrorText>
	)
}

export default TextError
