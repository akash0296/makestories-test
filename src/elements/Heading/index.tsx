import React from 'react'
import { Head } from './Heading.styled'

const Heading = ({ text }: { text: string }) => {
	return (
		<Head>
			<h1>{text}</h1>
		</Head>
	)
}

export default Heading
